import React, { useState, useEffect } from "react";
import Form from "./components/Form";
import GuestList from "./components/GuestList";
import Wrapper from "./components/Wrapper";
import Search from "./components/Search";
import axios from "axios";
import { v1 as uuid } from "uuid";

function App() {
  const [visitors, setVisitors] = useState([]);
  const [creatorId] = useState(uuid());
  const [formData, setFormData] = useState({
    name: "",
    message: "",
    creator_id: creatorId
  });

  const getVisitors = () => {
    axios
      .get(`http://localhost:3001/visitors?creator_id=${creatorId}`)
      .then(response => setVisitors(response.data))
      .catch(error => console.error(error));
  };

  useEffect(() => {
    getVisitors();
  }, []);

  return (
    <Wrapper>
      <Search reset={setVisitors} creatorId={creatorId}></Search>
      <Form
        formData={formData}
        setFormData={setFormData}
        creatorId={creatorId}
        reset={getVisitors}
      ></Form>
      <GuestList
        visitors={visitors}
        reset={getVisitors}
        creatorId={creatorId}
      ></GuestList>
    </Wrapper>
  );
}

export default App;
