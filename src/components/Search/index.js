import React, { useState } from "react";
import axios from "axios";
import { StyledForm } from "./style";

const Search = ({ reset, creatorId }) => {
  const [query, setQuery] = useState("");

  const getVisitors = () => {
    axios
      .get(
        `http://localhost:3001/visitors/search?q=${query}&creator_id=${creatorId}`
      )
      .then(response => reset(response.data))
      .catch(error => console.error(error));
  };

  const handleInputChange = e => {
    const { value } = e.target;
    setQuery(value);
  };

  const handleSubmit = e => {
    e.preventDefault();
    getVisitors();
  };

  return (
    <StyledForm onSubmit={handleSubmit}>
      <h3>Search the Guestlist</h3>
      <label htmlFor="q">
        <input
          type="text"
          name="q"
          value={query}
          onChange={handleInputChange}
        />
      </label>
      <button type="submit">Search</button>
    </StyledForm>
  );
};

export default Search;
