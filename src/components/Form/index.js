import React, { useState } from "react";
import { StyledForm } from "./style";
import axios from "axios";

const Form = ({ formData, setFormData, reset }) => {
  const [submitted, setSubmitted] = useState(false);

  const handleInputChange = e => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = e => {
    e.preventDefault();
    if (!!formData.name && !!formData.message) {
      axios
        .post("http://localhost:3001/visitors", formData)
        .then(response => {
          setSubmitted(true);
          reset();
        })
        .catch(error => console.error(error));
    }
  };

  return (
    !submitted && (
      <StyledForm onSubmit={handleSubmit}>
        <h3>Add a Comment to the Guestlist</h3>
        <label htmlFor="message">
          <textarea
            rows="5"
            cols="33"
            placeholder="Message"
            type="text"
            name="message"
            value={formData.message}
            onChange={handleInputChange}
          />
        </label>
        <label htmlFor="name">
          <input
            placeholder="Name"
            type="text"
            name="name"
            value={formData.name}
            onChange={handleInputChange}
          />
        </label>
        <button type="submit">Submit</button>
      </StyledForm>
    )
  );
};

export default Form;
