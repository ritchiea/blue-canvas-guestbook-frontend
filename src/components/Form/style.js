import styled from "styled-components";

export const StyledForm = styled.form`
  padding: 0 10% 2% 10%;

  label {
    display: block;
  }
`;
