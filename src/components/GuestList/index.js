import React from "react";
import { List, Text, ListItem, Header, DateText } from "./style";
import axios from "axios";

const GuestList = ({ visitors, creatorId, reset }) => {
  const deleteItem = visitor => {
    axios
      .delete(
        `http://localhost:3001/visitors/${visitor.id}?creator_id=${visitor.creator_id}`
      )
      .then(response => {
        reset();
      })
      .catch(error => console.error(error));
  };
  return (
    <>
      <Header>Guests</Header>
      <List>
        {visitors.map((visitor, index) => {
          return (
            <ListItem key={`${visitor.name}-${visitor.created_at}-${index}`}>
              <form onSubmit={() => deleteItem(visitor)}>
                <p>
                  <Text>{visitor.message}</Text>
                  <Text>- {visitor.name}</Text>
                </p>
                <DateText>{visitor.created_at}</DateText>
                {visitor.creator_id === creatorId && (
                  <button type="submit">Delete</button>
                )}
              </form>
            </ListItem>
          );
        })}
      </List>
    </>
  );
};

export default GuestList;
