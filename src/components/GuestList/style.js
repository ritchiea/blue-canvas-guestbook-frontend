import styled from "styled-components";

export const List = styled.ul`
  list-style-type: none;
  padding: 0 10% 10% 10%;
`;

export const Text = styled.span`
  margin-right: 1em;
`;

export const ListItem = styled.li`
  margin-bottom: 1em;
`;

export const Header = styled.h3`
  padding-left: 10%;
`;

export const DateText = styled.p`
  color: #ccc;
  font-style: italic;
`;
