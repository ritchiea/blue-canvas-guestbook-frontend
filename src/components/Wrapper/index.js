import React from "react";
import styled from "styled-components";

const StyledWrapper = styled.div`
  padding: 5%;
`;

const Wrapper = ({ children }) => <StyledWrapper>{children}</StyledWrapper>;

export default Wrapper;
