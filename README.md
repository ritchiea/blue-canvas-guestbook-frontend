# README

MacOS setup instructions for the Guestbook frontend:

1. Install the latest version of yarn
2. Clone the project and run `yarn`
3. Run `yarn start`
